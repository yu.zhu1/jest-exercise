import array from './index';

beforeEach(() => {
  expect.hasAssertions();
});

test('数组包含Banana', () => {
  const result = array('Apple', 'Banana');
  const value = 'Banana';

  // <--start
  // TODO: 给出正确的assertion
  expect(result).toEqual(expect.arrayContaining(array(value)));
  // --end->
});
