import { sendBombSignal, dieTogether } from './nuclear-bomb';

test('请测试 - sendBombSignal 会向 bomb 函数传递 O_o 作为起爆指令', () => {
  const mocksendSignal = jest.fn().mockImplementation(signal => signal);
  sendBombSignal(mocksendSignal);
  expect(mocksendSignal).lastCalledWith("O_o");
});
